const express = require('express');
const bodyParser = require('body-parser');
const open = require('opn');
const sessions = require('./sessions');
const translations = require('./translations');
const app = express();
const createdSessions = [];

app.use(express.static('public'));
app.use(bodyParser.json());

function getSessions() {
    return sessions.concat(createdSessions).map((session, idx) => Object.assign({ id: idx + 1, attendeeIds: [] }, session));
}

app.get('/api/sessions', function(req, res) {
    const sessions = getSessions();

    if (!sessions.length) {
        return res.status(404).end();
    }

    res.json(sessions);
});

app.post('/api/sessions', function(req, res) {
    const fieldErrors = {};

    if (!req.body || !req.body.title || !req.body.title.length) {
        fieldErrors.title = 'field is required';
    } else if (req.body.title.toLowerCase().indexOf('dog') > -1) {
        fieldErrors.title = 'the word "dog" is blacklisted';
    }

    if (Object.keys(fieldErrors).length) {
        return res.status(400).json({
            title: 'validation failed',
            message: 'some of the provided fields are invalid',
            fieldErrors: fieldErrors
        });
    }

    createdSessions.push(req.body);
    console.log('creating new session: ', req.body);
    res.status(200).end();
});

app.get('/api/translations', function(req, res) {
    res.json(translations);
});

app.listen(3000);
open('http://localhost:3000');
