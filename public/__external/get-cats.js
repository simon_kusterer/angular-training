;(function(global) {

  var CATS = [
      { "id": 1, "name": "Bailey", "picture": "http://lorempixel.com/100/100/cats/1/" },
      { "id": 2, "name": "Cookie", "picture": "http://lorempixel.com/100/100/cats/2/"  },
      { "id": 3, "name": "Missy", "picture": "http://lorempixel.com/100/100/cats/3/"  },
      { "id": 4, "name": "Kitty", "picture": "http://lorempixel.com/100/100/cats/4/" },
      { "id": 5, "name": "Snowball", "picture": "http://lorempixel.com/100/100/cats/5/" },
      { "id": 6, "name": "Pumpkin", "picture": "http://lorempixel.com/100/100/cats/6/" },
      { "id": 7, "name": "Mittens", "picture": "http://lorempixel.com/100/100/cats/7/" },
      { "id": 8, "name": "Oscar", "picture": "http://lorempixel.com/100/100/cats/8/" },
      { "id": 9, "name": "Muffin", "picture": "http://lorempixel.com/100/100/cats/9/" },
      { "id": 10, "name": "Tinkerbell", "picture": "http://lorempixel.com/100/100/cats/10/"  }
  ];

  function getCats(cb) {
    setTimeout(function() {
      cb(CATS);
    }, 100);
  };

  global.getCats = getCats;

})(window);
