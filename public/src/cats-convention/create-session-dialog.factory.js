(function(angular) {
    'use strict';
    angular.module('cats-convention').factory('CreateSessionDialog', function(anDialog, anDialogUtils) {
        function CreateSessionDialog(dialog, SessionsApi, bbErrorUtils, anMessages) {
            this.dialog = dialog;
            this.SessionsApi = SessionsApi;
            this.bbErrorUtils = bbErrorUtils;
            this.anMessages = anMessages;

            this.serverError = null;
            this.data = {};
        }

        CreateSessionDialog.prototype.submit = function() {
            var that = this;

            return this.SessionsApi
                .createSession(this.data)
                .then(function(res) {
                    that.dialog.close(true);
                })
                .catch(function(res) {
                    that.bbErrorUtils.showErrorMessage(res, 'createSessionErrorContainer');
                    that.serverError = res.data;
                });
        };

        return {
            create: function(opts) {
                var defaults = {
                    controller: ['dialog', 'SessionsApi', 'bbErrorUtils', 'anMessages', CreateSessionDialog],
                    contentTemplate: 'src/cats-convention/create-session-dialog.html'
                };

                return anDialog.create(anDialogUtils.extendOptions(defaults, opts));
            }
        };
    });
})(angular);
