(function(angular) {
    'use strict';
    class OverviewController {
        constructor(SessionsApi, CreateSessionDialog, $q, $timeout) {
            this.SessionsApi = SessionsApi;
            this.CreateSessionDialog = CreateSessionDialog;
            this.$q = $q;
            this.$timeout = $timeout;

            this.sessions = [];
            this.attendeesCount = 0;
        }

        $onInit() {
            this.init();
        }

        init() {
            var that = this;
            var getCatsPromise = this._getCats();

            var getSessionsPromise = this.SessionsApi.getSessions().catch(function(res) {
                // if the endpoint returns with status 404 then treat that as empty list
                if (res.status === 404) {
                    return { data: [] };
                }

                // ...otherwise reject, so the error can be handled in a later catch block
                return that.$q.reject(res);
            });

            // wait for all promises to resolve
            return this.$q
                .all([getSessionsPromise, getCatsPromise])
                .then(function(results) {
                    var sessions = results[0].data;
                    var cats = results[1];

                    return that._addAttendeesToSessions(sessions, cats);
                })
                .then(function(sessions) {
                    var attendeesCount = sessions.reduce((count, session) => count + session.attendees.length, 0);

                    // attach the sessions to the controller instance
                    that.sessions = sessions;
                    that.attendeesCount = attendeesCount;
                })
                .catch(function(err) {
                    // if any promise rejects, we will immediately land here.
                    console.log('init error', err);
                });
        }

        // open the dialog for creating a new session
        createSession() {
            var that = this;

            this.CreateSessionDialog.create({
                onClose: function(isDirty) {
                    if (isDirty) {
                        that.init();
                    }
                }
            });
        }

        // the sessions returned by the endpoint only contain attendeeIds by default.
        // we will add another property called 'attendees' to the session-object
        // which holds an array of cats which are attending that session.
        _addAttendeesToSessions(sessions, cats) {
            // create a map from id to cat
            var catsById = _.keyBy(cats, 'id');

            return sessions.map(function(session) {
                var attendees = session.attendeeIds
                    // filter out non existing cats
                    .filter(attendeeId => attendeeId in catsById)
                    // map from attendeeId to cat
                    .map(attendeeId => catsById[attendeeId]);

                return Object.assign({}, session, { attendees: attendees });
            });
        }

        // window.getCats() uses a callback by default.
        // let's make it promise-based instead.
        _getCats() {
            return this.$q(function(resolve) {
                window.getCats(function(cats) {
                    resolve(cats);
                });
            });
        }
    }

    angular.module('cats-convention').component('overview', {
        controllerAs: 'overviewCtrl',
        templateUrl: 'src/cats-convention/overview.html',
        controller: OverviewController
    });
})(angular);
