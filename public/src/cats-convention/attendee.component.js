(function(angular) {
    'use strict';
    angular.module('cats-convention').component('attendee', {
        templateUrl: 'src/cats-convention/attendee.html',
        bindings: {
            cat: '<'
        }
    });
})(angular);
