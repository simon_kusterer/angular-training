(function(angular) {
    angular.module('cats-convention', ['ngRoute', 'ngSanitize', 'k15t.auiNg']).config([
        '$routeProvider',
        function($routeProvider) {
            $routeProvider.when('/', {
                template: '<overview></overview>'
            });
        }
    ]);
})(angular);
