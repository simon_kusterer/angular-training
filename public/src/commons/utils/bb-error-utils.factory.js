(function(angular) {
    'use strict';
    angular.module('app').factory('bbErrorUtils', function($translate, $q, anMessageDialog, anMessages, anDialogUtils, $window) {
        var isErrorResponse = function(res) {
            return !!(res.status >= 400 && res.status <= 599);
        };

        var isConflictResponse = function(res) {
            return res.status === 409;
        };

        var isAsyncTaskErrorResponse = function(res) {
            // TODO: figure out if there is a better way for checking
            // We can't seem to depend on the status-code here.
            return !!(res.data.finished && res.data.hasFailed && res.data.failureMessage);
        };

        var hasFieldErrors = function(res) {
            return !!(angular.isObject(res.data.fieldErrors) && Object.keys(res.data.fieldErrors).length);
        };

        var responseParsers = {
            // parse an error response and try
            // to extract an title and message
            common: function(res) {
                if (!isErrorResponse(res)) {
                    return false;
                }

                var ret = {};

                if (angular.isString(res.data.message)) {
                    ret.message = res.data.message;
                } else {
                    ret.message = JSON.stringify(res.data);
                }

                if (angular.isString(res.data.title)) {
                    ret.title = res.data.title;
                }

                return ret;
            },

            withFieldErrors: function(res) {
                if (!isErrorResponse(res)) {
                    return false;
                }

                var ret = {};

                if (angular.isString(res.data.message)) {
                    ret.message = res.data.message;

                    if (angular.isArray(res.data.errors) && res.data.errors.length) {
                        var fieldErrors = res.data.errors
                            .map(function(error) {
                                var msg = error.message;

                                if (error.path && error.path !== '') {
                                    msg += ' (' + error.path + ')';
                                }

                                return '<li>' + msg + '</li>';
                            })
                            .join('');

                        ret.details = '<ul>' + fieldErrors + '</ul>';
                    }
                } else {
                    ret.message = JSON.stringify(res.data);
                }

                if (angular.isString(res.data.title)) {
                    ret.title = res.data.title;
                }

                return ret;
            },

            // parse an error response from an asyncTask
            asyncTask: function(res) {
                if (!isAsyncTaskErrorResponse(res)) {
                    return false;
                }

                var failureMessage = res.data.failureMessage;
                var ret = {};

                if (angular.isString(failureMessage)) {
                    ret.message = failureMessage;
                } else if (angular.isObject(failureMessage) && failureMessage.message !== null) {
                    ret.message = failureMessage.message;
                    ret.details = failureMessage.stackTrace;
                } else {
                    ret.message = JSON.stringify(res.data);
                }

                if (res.data.title) {
                    ret.title = res.data.title;
                }

                return ret;
            }
        };

        var getParserFromResponse = function(res) {
            if (isAsyncTaskErrorResponse(res)) {
                return 'asyncTask';
            } else {
                return 'common';
            }
        };

        // takes a response as input and creates an message-object
        // out of it. the message-object can then be used within an
        // error-dialog or an error-container.
        var createMessageFromResponse = function(res, parser) {
            var defaults = {
                severity: 'error',
                styleDetails: {
                    'word-wrap': 'break-word'
                },
                styleMessageWrapper: {
                    'max-height': '100px',
                    'overflow-y': 'auto'
                }
            };

            if (angular.isString(parser)) {
                parser = responseParsers[parser];
            } else {
                // try to guess the parser..
                parser = responseParsers[getParserFromResponse(res)];
            }

            var msg = parser(res);

            if (!msg) {
                return false;
            }

            return angular.extend({}, defaults, msg);
        };

        // open up an error-dialog.
        var createErrorDialog = function(res, opts, parser) {
            var msg = createMessageFromResponse(res, parser);

            if (!msg) {
                return false;
            }

            return createErrorDialogByMessage(msg, opts);
        };

        var createErrorDialogByMessage = function(msg, opts) {
            var defaults = {
                locals: {
                    labels: {
                        title: $translate.instant('bb.common.words.Error')
                    },
                    styles: {
                        dialog: {
                            width: '800px'
                        }
                    }
                }
            };

            return anMessageDialog.create(msg, angular.extend({}, defaults, opts));
        };

        // pushes a message into an message-container.
        var showErrorMessage = function(res, target, parser) {
            var msg = createMessageFromResponse(res, parser);

            if (!msg) {
                return false;
            }

            anMessages.clearMessages(target);
            return anMessages.addMessage(target, msg);
        };

        var createConflictDialog = function(res, opts) {
            anMessageDialog.create(
                createMessageFromResponse(res),
                anDialogUtils.extendOptions(
                    {
                        onClose: function() {
                            // hard reset...
                            $window.location.reload();
                        },
                        locals: {
                            labels: {
                                title: $translate.instant('bb.common.words.Error')
                            },
                            styles: {
                                dialog: {
                                    width: '800px'
                                }
                            }
                        }
                    },
                    opts
                )
            );
        };

        // this handler opens up an conflict-response dialog
        // in case the server returns with the status code 409.
        // otherwise the response will be passed on to the
        // next error handler.
        var createConflictDialogHandler = function(passThrough) {
            return function(res) {
                return $q(function(resolve, reject) {
                    if (!isConflictResponse(res)) {
                        return reject(res);
                    }
                    createConflictDialog(res);
                    if (passThrough) {
                        return reject(res);
                    }
                    resolve();
                });
            };
        };

        var createErrorDialogHandler = function(passThrough) {
            return function(res) {
                return $q(function(resolve, reject) {
                    if (!isErrorResponse(res)) {
                        return reject(res);
                    }
                    createErrorDialog(res);
                    if (passThrough) {
                        return reject(res);
                    }
                    resolve();
                });
            };
        };

        var createErrorMessageHandler = function(target, passThrough, parser) {
            return function(res) {
                return $q(function(resolve, reject) {
                    if (!isErrorResponse(res)) {
                        return reject(res);
                    }
                    showErrorMessage(res, target, parser);
                    if (passThrough) {
                        return reject(res);
                    }
                    resolve();
                });
            };
        };

        var createErrorPassThroughHandler = function(fn) {
            return function(res) {
                return $q(function(resolve, reject) {
                    if (!isErrorResponse(res)) {
                        return reject(res);
                    }
                    fn(res);
                    reject(res);
                });
            };
        };

        return {
            isErrorResponse: isErrorResponse,
            isAsyncTaskErrorResponse: isAsyncTaskErrorResponse,
            isConflictResponse: isConflictResponse,
            hasFieldErrors: hasFieldErrors,
            createMessageFromResponse: createMessageFromResponse,
            createErrorDialog: createErrorDialog,
            createErrorDialogByMessage: createErrorDialogByMessage,
            createConflictDialog: createConflictDialog,
            showErrorMessage: showErrorMessage,
            createConflictDialogHandler: createConflictDialogHandler,
            createErrorDialogHandler: createErrorDialogHandler,
            createErrorMessageHandler: createErrorMessageHandler,
            createErrorPassThroughHandler: createErrorPassThroughHandler
        };
    });
})(angular);
