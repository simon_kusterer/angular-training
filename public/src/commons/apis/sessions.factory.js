(function(angular) {
    angular.module('app').service('SessionsApi', Api);

    function Api($http, $q) {
        this.$http = $http;
        this.$q = $q;
    }

    Api.prototype.getSessions = function() {
        return this.$http.get('/api/sessions');
    };

    Api.prototype.createSession = function(session) {
        return this.$http.post('/api/sessions', session);
    };
})(angular);
