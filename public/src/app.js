(function(angular) {
    angular.module('app', ['cats-convention', 'ngRoute', 'pascalprecht.translate']);
})(angular);
